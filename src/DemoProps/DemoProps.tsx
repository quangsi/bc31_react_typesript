import React from "react";
import UserInfor from "./UserInfor";

export interface userInterface {
  username: string;
  age: number;
}

let userInfor: userInterface = {
  username: "Alice",
  age: 18,
};

export default function DemoProps() {
  return (
    <div>
      <p>DemoProps</p>

      <UserInfor user={userInfor} />
    </div>
  );
}
