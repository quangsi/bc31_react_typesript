import React from "react";
import { userInterface } from "./DemoProps";

interface InterfaceUserComponent {
  user: userInterface;
}

export default function UserInfor({ user }: InterfaceUserComponent) {
  return (
    <div>
      <p>UserInfor</p>

      <p>Username: {user.username}</p>
      <p>Age: {user.age}</p>
    </div>
  );
}
