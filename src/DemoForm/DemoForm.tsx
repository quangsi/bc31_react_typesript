import React, { useState } from "react";

export default function DemoForm() {
  const [todo, setTodo] = useState<string>("");
  let handleOnChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
  };
  return (
    <div className="container py-5">
      <div className="form-group">
        <input
          type="text"
          className="form-control"
          placeholder="Todo name"
          onChange={(e) => {
            handleOnChangeInput(e);
          }}
        />
      </div>
      <button className="btn btn-danger">Add todo</button>
    </div>
  );
}
