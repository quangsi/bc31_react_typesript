export interface InterfaceTodo {
  id: string;
  text: string;
  isCompleted: boolean;
}

export interface InterfaceTodoComponent {
  todo: InterfaceTodo;
  handleTodoRemove: (id: string) => void;
}

export interface InterfaceTodoListComponent {
  todos: InterfaceTodo[];
  handleTodoRemove: (id: string) => void;
}
export interface InteraceFormComponent {
  handleTodoCreate: (todo: InterfaceTodo) => void;
}
