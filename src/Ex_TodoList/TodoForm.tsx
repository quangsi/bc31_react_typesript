import React, { useState } from "react";
import { InteraceFormComponent, InterfaceTodo } from "./InterfaceTodoList";

// @ts-ignore
import shortid from "shortid";
export default function TodoForm({ handleTodoCreate }: InteraceFormComponent) {
  const [todoTitle, setTodoTitle] = useState<string>("");

  let handleOnChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);

    setTodoTitle(e.target.value);
  };

  let handleAddTodo = () => {
    let newTodo: InterfaceTodo = {
      id: shortid.generate(1000),
      text: todoTitle,
      isCompleted: false,
    };
    handleTodoCreate(newTodo);
  };

  return (
    <div className="flex space-x-5">
      <input
        onChange={handleOnChangeInput}
        type="text"
        className="p-5 rounded border-gray-500 border flex-grow placeholder-orange-600 "
        placeholder="Type todo titile"
        value={todoTitle}
      />

      <button
        onClick={handleAddTodo}
        className="bg-orange-600 px-5 py-2 text-white rounded flex-shrink-0"
      >
        Add todo
      </button>
    </div>
  );
}
