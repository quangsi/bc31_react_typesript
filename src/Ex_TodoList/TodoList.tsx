import React from "react";
import { InterfaceTodoListComponent } from "./InterfaceTodoList";
import TodoItem from "./TodoItem";

export default function TodoList({
  todos,
  handleTodoRemove,
}: InterfaceTodoListComponent) {
  console.log("todos: ", todos);

  let renderContentTable = () => {
    return todos.map((todo) => {
      return <TodoItem handleTodoRemove={handleTodoRemove} todo={todo} />;
    });
  };
  return (
    <div>
      <div className="overflow-x-auto relative">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="py-3 px-6">
                Id
              </th>
              <th scope="col" className="py-3 px-6">
                Titile
              </th>
              <th scope="col" className="py-3 px-6">
                Is_Completed
              </th>
            </tr>
          </thead>
          <tbody>{renderContentTable()}</tbody>
        </table>
      </div>
    </div>
  );
}
