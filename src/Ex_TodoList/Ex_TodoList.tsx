import React, { useState } from "react";
import { InterfaceTodo } from "./InterfaceTodoList";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";

export default function Ex_TodoList() {
  const [todos, setTodos] = useState<InterfaceTodo[]>([
    {
      id: "1",
      text: "Do homework",
      isCompleted: false,
    },

    {
      id: "2",
      text: "Workout",
      isCompleted: false,
    },
  ]);

  let handleTodoCreate = (todo: InterfaceTodo) => {
    let newTodos = [...todos, todo];
    setTodos(newTodos);
  };

  let handleTodoRemove = (idTodo: string) => {
    let newTodo = todos.filter((todo: InterfaceTodo) => {
      return todo.id !== idTodo;
    });

    setTodos(newTodo);
  };

  return (
    <div className="container mx-auto py-5 space-y-10">
      <TodoForm handleTodoCreate={handleTodoCreate} />
      <TodoList handleTodoRemove={handleTodoRemove} todos={todos} />
    </div>
  );
}
