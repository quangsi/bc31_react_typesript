import React, { useState } from "react";
interface InterfaceAddress {
  number: string;
  district: string;
  city: string;
}

interface InterfaceUser {
  username: string;
  age: number;
  address: InterfaceAddress;
}

let dataUser: InterfaceUser = {
  username: "Alice",
  age: 18,
  address: {
    number: "112 PVD",
    district: "Quận Thủ Đức",
    city: "Hồ Chí Minh",
  },
};

export default function DemoState() {
  const [like, setLike] = useState<number>(1);
  const [user, setUser] = useState<InterfaceUser>(dataUser);
  const handlePlusLike = () => {
    setLike(like + 1);
  };
  return (
    <div>
      <p>
        like:{like}{" "}
        <button className="btn btn-success" onClick={handlePlusLike}>
          Plus like
        </button>
      </p>
    </div>
  );
}
