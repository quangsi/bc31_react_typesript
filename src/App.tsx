import React from "react";
import logo from "./logo.svg";
import "./App.css";
import DemoProps from "./DemoProps/DemoProps";
import DemoState from "./DemoState/DemoState";
import DemoForm from "./DemoForm/DemoForm";
import Ex_TodoList from "./Ex_TodoList/Ex_TodoList";

function App() {
  return (
    <div className="App">
      {/* <DemoProps /> */}
      {/* <DemoState /> */}
      {/* <DemoForm /> */}
      <Ex_TodoList />
    </div>
  );
}

export default App;
